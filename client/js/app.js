/**
 * Client side code.
 */
(function() {               /* this is IIFie declaration */
    var app = angular.module("RegApp", []);  /* registering RegApp as a function to Angular to recognise */

    app.controller("RegistrationCtrl", ["$http", RegistrationCtrl]);  /* $http is a convention from Angular which is to inject more input info. then RegistrationCtrl is to register ITSELF*/

    function RegistrationCtrl($http) {
        var user = this; // vm

        self.user = {
            email: "",
            password: "",
            confirmpassword:"",
            fullname:"",
            gender:"",
            dateOfBirth:"",
            address:"",
            nationality:"",
            contactNumber:""
        };

        self.displayUser = {
            email: "",
            password: "",
            confirmpassword:"",
            fullname:"",
            gender:"",
            dateOfBirth:"",
            address:"",
            nationality:"",
            contactNumber:""
        };

        // self.registerUser = function() {
        //     console.log(self.user.email);
        //     console.log(self.user.password);
        //     $http.post("/users", self.user) /* calling the endpoint, then is to capture the data within */
        //         .then(function(result) {
        //             console.log(result);
        //             self.displayUser.email = result.data.email;
        //             self.displayUser.password = result.data.password;
        //         }).catch(function(e) {
        //             console.log(e);
        //         });
        // };

            self.registerUser = function() {
            console.log(self.user.email);
            console.log(self.user.password);
            console.log(self.user.confirmpassword);
            console.log(self.user.fullname);
            console.log(self.user.gender);
            console.log(self.user.dateOfBirth);
            console.log(self.user.address);
            console.log(self.user.nationality);
            console.log(self.user.contactNumber);
            $http.post("/users", self.user) /* calling the endpoint, then is to capture the data within */
                .then(function(result) {
                    console.log(result);
                    self.displayUser.email = result.data.email;
                    self.displayUser.password = result.data.password;
                    self.displayUser.confirmpassword = result.data.confirmpassword;
                    self.displayUser.fullname = result.data.fullname;
                    self.displayUser.gender = result.data.gender;
                    self.displayUser.dateOfBirth = result.data.dateOfBirth;
                    self.displayUser.address = result.data.address;
                    self.displayUser.nationality = result.data.nationality;
                    self.displayUser.contactNumber = result.data.contactNumber;

                }).catch(function(e) {
                    console.log(e);
                });
        };

    }

})();