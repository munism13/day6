/**
 * Server side code.
 */
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser"); /* body parser to interpret the data if not it will be passed on without actions */

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));/* ENGAGE is an Express action to algorithm to disable. To turn it to truth Express will act on complex json data */
app.use(bodyParser.json()); /* .json is to enable Express to act on only the data within */

console.log(__dirname);
console.log(__dirname + "/../client/"); 
const NODE_PORT = process.env.NODE_PORT || 3001;
var y = 0;

app.use(express.static(__dirname + "/../client/")); /* directs where the static and Angular files are  */
/*
app.use('/bower_components', express.static(__dirname +
    "/../client/bower_components"));*/

if (!NODE_PORT) {
    console.log("Express Port is not set"); /* Dont need this if there is a fall back default definiteion of port 3000 */
    process.exit();
}

function sum(x, z) {
    var y = x + z;
    return y;
}



app.get("/students", function(req, res) {   /* .students is the end point  */
    console.log("students");
    var person1 = {
        name: "Kenneth",
        age: 35
    };
    var person2 = {
        name: "Alvin",
        age: 40
    };
    var users = [person1, person2, person1, person1];
    res.send("My name is " + JSON.stringify(users[0]));
});

app.get("/users", function(req, res) {
    console.log("users");
    var person1 = {
        name: "Kenneth",
        age: 35
    };
    var person2 = {
        name: "Alvin",
        age: 40
    };
    var users = [person1, person2, person1, person1];
    res.json(users); /* this requests for all 'users' object */
});

app.post("/users", function(req, res) {
    console.log("Received user obejct " + req.body);
    console.log("Received user obejct " + JSON.stringify(req.body));
    var user = req.body;
    user.email = "hi " + user.email;
    console.log("email > " + user.email);
    console.log("password > " + user.password);
    console.log("confirm-password >" + user.confirmpassword);
    console.log("fullname >" + user.fullname);
    console.log("gender >" + user.gender);
    console.log("dateOfBirth >" + user.dateOfBirth);
    console.log("address >" + user.address);
    console.log("nationality >" + user.nationality);
    console.log("contactNumber >" + user.contactNumber);
    res.status(200).json(user); /*this returns to Anuglar, if not does not respond*/
});

app.use(function(req, res, next) {
    y = 20 / 5;
    //res.send("<h1>Before the wrong door ! " + y + "</h1>");
    next();
});

app.use(function(req, res, next) {
    console.log(" sorry wrong door -> ");
    var x = sum(1, y);
    //res.send("<h1>Sorry wrong door ! " + x + "</h1>");
    next();
});

app.use(function(req, res) {
    console.log(" sorry wrong door -> ");
    var x = sum(1, y);
    res.send("<h1>!!!! Page not found ! ! " + x + "</h1>");
});

app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});